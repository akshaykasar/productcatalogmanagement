package com.pcm.dao.impl;

import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pcm.bean.Product;
import com.pcm.dao.AbstractDao;
import com.pcm.util.ProductDBInterfaceImpl;

public class ProductDaoImpl extends AbstractDao<Product> {

	Logger logger = LoggerFactory.getLogger(ProductDaoImpl.class); 
	
	public static int maxId;
	public ProductDaoImpl() {
		dbConnector = ProductDBInterfaceImpl.getInstance();
		try{
			ArrayList<Product> list = dbConnector.readData();
			if(list == null){
				maxId = 0;
			}else{
				Collections.sort(list);
				maxId = list.get(list.size()-1).getId();
			}
			logger.info("\nMax Id: "+maxId);
			
		}catch(Exception e){
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public Product getById(int id) throws Exception {
		ArrayList<Product> list = dbConnector.readData();
		if(list != null && !list.isEmpty()){
			for(Product obj : list){
				if(obj.getId() == id){
					return obj;
				}
			}
		}
		return null;
	}

	@Override
	public void deleteById(int id) throws Exception{
		ArrayList<Product> list = this.getAll();
		//ArrayList<Product> list = dbConnector.readData();
		// FIXME which of the above two is more appropriate?

		if(list != null && !list.isEmpty()){
			for(Product obj : list){
				if(obj.getId() == id){
					list.remove(obj);
					break;
				}
			}
			dbConnector.writeData(list);
		}
	}

	@Override
	public void update(Product product) throws Exception{
		ArrayList<Product> list = this.getAll();
		//ArrayList<Product> list = dbConnector.readData();
		// FIXME which of the above two is more appropriate?

		if(list != null && !list.isEmpty()){
			for(Product obj : list){
				if(obj.getId() == product.getId()){
					list.remove(obj);
					break;
				}
			}
		}
		list.add(product);
		dbConnector.writeData(list);

	}

	@Override
	public void save(Product obj) throws Exception{
		//function to add new object in the file
		obj.setId(++maxId);
		super.save(obj);
	}

	@Override	
	public ArrayList<Product> getAll() throws Exception{
		//return an arraylist of all the objects in the file
		return super.getAll();
	}

}
