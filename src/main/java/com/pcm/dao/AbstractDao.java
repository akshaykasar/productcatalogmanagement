package com.pcm.dao;

import java.util.ArrayList;

import com.pcm.util.DBInterface;

public abstract class AbstractDao<T>{

	protected DBInterface<T> dbConnector;

	public abstract T getById(int id)throws Exception;
	/*
	 * {
	 *
		//TODO function to fetch from file the object T with id i
		*
		 * This cant be implemented in the abstract class 
		 * as the object of T class does not exist, yet.
		 * 
		 * ArrayList<T> list = dbConnector.readData();
		if(list != null && !list.isEmpty()){
			for(T obj : list){
				if()
			}
		}
		return null;
	}
	*/

	public void save(T obj) throws Exception{
		//function to add new object in the file
		if(obj != null){
			ArrayList<T> list = dbConnector.readData();
			list = list == null ? new ArrayList<T>() : list;
			list.add(obj);
			dbConnector.writeData(list);
		}
	}

	public abstract void deleteById(int id) throws Exception;

	public abstract void update(T obj) throws Exception;

	public ArrayList<T> getAll() throws Exception{
		//return an arraylist of all the objects in the file
		ArrayList<T> list = dbConnector.readData();
		return list;
	}

}
