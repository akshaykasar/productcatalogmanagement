package com.pcm.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pcm.config.AppInitializer;
import com.pcm.service.AcceptDataForProductService;
import com.pcm.service.AcceptDataService;


public class PCMController {
	
	public static void initProductControls(){
		try{
			
			Logger logger = LoggerFactory.getLogger(PCMController.class);
		    
			AppInitializer.getInstance();
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
			int input = 0;
			AcceptDataService acceptDataService = new AcceptDataForProductService();

			while(true){
				System.out.println("******PRODUCT*******");
				System.out.print("Choose from:\""
						+ "\n1.\t Show All products.\""
						+ "\n2\t Add new Product.\""
						+ "\n3\t Delete Product.\""
						+ "\n4\t Update Product.\""
						+ "\n0\t Exit.");

				System.out.print("\nEnter your Choise: \t");
				input = bufferReader.readLine().charAt(0);

				switch(input){
				case '1':	acceptDataService.toDisplayAll();					
					break;

				case '2':	acceptDataService.toAdd();
					break;
				
				case '3':	acceptDataService.toDelete();
					break;
				
				case '4':	acceptDataService.toUpdate();				
					break;
					
				case '0':
					logger.info("Terminating Application...!");
					return;
					
				default: System.out.println("Invalid input!");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
