package com.pcm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pcm.bean.Product;
import com.pcm.dao.impl.ProductDaoImpl;
import com.pcm.exception.InputValidator;

public class AcceptDataForProductService implements AcceptDataService{

	private ProductDaoImpl productDaoImpl;

	Logger logger = LoggerFactory.getLogger(AcceptDataForProductService.class);

	public AcceptDataForProductService() {
		productDaoImpl = new ProductDaoImpl();
	}

	public void toDisplayAll() throws Exception {
		ArrayList<Product> list = productDaoImpl.getAll();
		if( list != null ){ 
			System.out.println("Displaying all the products:\n"+list.toString());
		}else{
			System.out.println("Product DB is empty");
		}

	}

	public void toAdd() throws Exception {
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Adding product to the store\""
				+ "\nEnter Name:");
		String name = null, cost = null, sellingPrice = null;
		name = bufferReader.readLine();
		System.out.print("\nEnter Cost:");
		cost = bufferReader.readLine();
		System.out.print("\nEnter SellingPrice:");
		sellingPrice = bufferReader.readLine();

		if(name == null || cost == null || sellingPrice == null){
			logger.info("Values for new Product cannot be null!");
			return;
		}

		Product product = new Product();
		product.setName(name);
		product.setCost(new InputValidator().isValidUnsignedDouble(cost));
		product.setSellingPrice(new InputValidator().isValidUnsignedDouble(sellingPrice));
		productDaoImpl.save(product);

	}

	public void toDelete() throws Exception {
		System.out.println("Enter the id of the Product to be deleted:");
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
		String deleteId=bufferReader.readLine();
		productDaoImpl.deleteById(new InputValidator().isValidUnsignedInteger(deleteId));
	}

	public void toUpdate() throws Exception{
		String name = null, cost = null, sellingPrice = null;
		Product product;
		System.out.println("Enter the id of the Product to be updated:");

		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
		String updateId=bufferReader.readLine();
		Product prevProduct = null;
		prevProduct = productDaoImpl.getById(new InputValidator().isValidInteger(updateId));
		logger.info(prevProduct.toString());
		
		System.out.println("Enter the new details of the product:\n");
		name = bufferReader.readLine();
		System.out.print("\nEnter Cost:");
		cost = bufferReader.readLine();
		System.out.print("\nEnter SellingPrice:");
		sellingPrice = bufferReader.readLine();

		if(name == null && cost == null){
			logger.info("No values entered. Product Updation Cancelled.");
			return;
		}else if(name == null && cost != null){
			name = prevProduct.getName();
		}else if(name != null && cost == null){
			cost = prevProduct.getCost()+"";
		}

		product = new Product();
		product.setCost(new InputValidator().isValidUnsignedDouble(cost));
		product.setSellingPrice(new InputValidator().isValidUnsignedDouble(sellingPrice));
		product.setName(name);
		productDaoImpl.update(product);

	}

	public void toIncreaseProductProfit() throws Exception{

	}

}
