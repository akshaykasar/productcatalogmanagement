package com.pcm.service;

public interface AcceptDataService {
	public void toDisplayAll() throws Exception;
	public void toAdd() throws Exception;
	public void toDelete() throws Exception;
	public void toUpdate() throws Exception;
}
