package com.pcm.handler;

import com.pcm.controller.PCMController;

/**
 * main class where the flow begins
 * @author akshay
 *
 */
public class MainClass {

	public static void main(String args[]){
		PCMController.initProductControls();
	}
}
