package com.pcm.bean;

import java.io.Serializable;

/**
 * Bean class for Products
 * @author akshay
 *
 */
public class Product implements Serializable, Comparable<Product>{
	private int id;
	private String name;
	private double cost;
	private double sellingPrice;
	/*
	 * Commenting temporarily to test the code execution - 8/09/2017-0908
	 * 
	 * private String code;
	private Date date;
	private String description;
	private String category;
	private ArrayList<String> tags;*/

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
/*	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public ArrayList<String> getTags() {
		return tags;
	}
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}*/

	public String toString(){
		return "[ID: "+this.id+", "
				+ "NAME: "+this.name+", "
//				+ "CODE: "+this.code+", "
//				+ "DATE: "+this.date.toString()+", "
//				+ "DESCRIPTION: "+this.description+", "
//				+ "CATEGORY: "+this.category+", "
				+ "COST: "+this.cost+", "
				+ "SELLING_PRICE: "+this.sellingPrice+", "
//				+ "TAGS: "+this.tags.toString()
				+ "]";
		}
	public int compareTo(Product o) {
		return this.id - o.id;
	}
	public double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

}
