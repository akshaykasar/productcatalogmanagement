package com.pcm.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pcm.bean.Product;
import com.pcm.exception.ErrorCode;
import com.pcm.exception.PCMException;

/**
 * Implementation for reading and writing data into the database file for that particular product
 * @author akshay
 *
 */
public class ProductDBInterfaceImpl implements DBInterface<Product> {

	static final Logger logger = LoggerFactory.getLogger(ProductDBInterfaceImpl.class);

	private File dbFile;

	private static ProductDBInterfaceImpl instance;

	private ProductDBInterfaceImpl(){}

	public static ProductDBInterfaceImpl getInstance(){
		if(instance == null){
			instance = new ProductDBInterfaceImpl();
		}
		return instance;
	}
	/**
	 * function to read the dbfile and return an ArrayList of Products
	 * @return - ArrayList of Products read from the json file
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Product> readData() throws Exception {
		ArrayList<Product> productList = null;

		if(dbFile != null){
			logger.info("Reading dbfile product.json...");
			Gson gson = new GsonBuilder().create();
			if(!dbFile.exists()){
				logger.info("DB file does not Exist...\nCreating new db file...");
				dbFile.createNewFile();
			}
			FileReader reader = new FileReader(dbFile);
			Product[] productArray = gson.fromJson(reader,Product[].class);
			if(productArray == null){
				logger.info("Products DB file is empty!");
			}else{
				productList = new ArrayList<Product>(Arrays.asList(productArray));
				logger.debug("File content:\n"+productList.toString());
			}
			reader.close();
		}else{
			logger.info("DB file not found!");
			throw new PCMException(ErrorCode.FILE_NOT_FOUND, "Data Base File Missing!");
		}
		return productList;
	}

	/**
	 * function to write the ArrayList of Products to json file
	 * @param data - accepts ArrayList of Products to be written to the dbFile
	 */
	public void writeData(ArrayList<Product> data) throws Exception {
		String jsonObj = (new Gson()).toJson(data);
		FileWriter writer = new FileWriter(dbFile);;
		if(data != null && data.size() > 0){
			logger.info("Clearing the contents of the previous file..."+dbFile.length()); 
			//to delete all the contents of the file
			writer.write("");
			writer.close();

			logger.debug("Writing the following contents to the file:\n"+jsonObj);
			// writing all the contents again
			writer = new FileWriter(dbFile);
			writer.write(jsonObj);
			writer.close();
			logger.debug("Size of file after writing the content:"+dbFile.length());
			logger.debug("Size of file after writing the content:"+dbFile.getAbsolutePath());
		}else{
			logger.info("Empty Data cannot be written to the file!");
			//TODO through custom exception
		}
		
	}

	/*
	 * Commented as the dbFile should not be directly accessible as we are providing an interface
	 * 
	 * uncommenting the following as the dbfile is initited in a single ton class AppInitializer.
	 */
	/*public File getDbFile() {
		return dbFile;
	}*/

	public void setDbFile(File dbFile) {
		this.dbFile = dbFile;
	}

}
