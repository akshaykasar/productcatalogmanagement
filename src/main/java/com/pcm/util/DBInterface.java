package com.pcm.util;

import java.util.ArrayList;

/**
 * class to read and write the data to files in the give format.
 * @author akshay
 *
 */
public interface DBInterface<T> {
	public ArrayList<T> readData() throws Exception;
	public void writeData(ArrayList<T> data) throws Exception;	

}
