package com.pcm.config;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pcm.controller.PCMController;
import com.pcm.util.ProductDBInterfaceImpl;

/**
 * Singlton class to initialize the app resources eg. db file
 * @author akshay.kasar
 *
 */
public class AppInitializer {
	//create and initialize files
	Logger logger = LoggerFactory.getLogger(PCMController.class);
	
	private static AppInitializer instance;
	private static ProductDBInterfaceImpl pdi;
	
	private AppInitializer(){
		String path = System.getProperty("user.dir")+System.getProperty("file.separator")+"product.json";
		
		logger.info("DB file path: "+path);
	    File product = new File(path);
	    ProductDBInterfaceImpl.getInstance().setDbFile(product);
	    
	}
	
	public static AppInitializer getInstance(){
		if(instance == null){
			instance = new AppInitializer();
		}
		return instance;
	}

	public static ProductDBInterfaceImpl getPdi() {
		return pdi;
	}

	public static void setPdi(ProductDBInterfaceImpl pdi) {
		AppInitializer.pdi = pdi;
	}
	
}
