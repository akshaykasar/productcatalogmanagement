package com.pcm.exception;

public interface Validator {
	public Integer isValidInteger(String input);
	public Double isValidDouble(String input);

}
