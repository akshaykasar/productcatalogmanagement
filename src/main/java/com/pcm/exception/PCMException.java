package com.pcm.exception;

public class PCMException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int errorCode;
	String message;
	
	public PCMException(ErrorCode errorCode, String msg) {
		super("Error ("+errorCode+") : "+msg);
	}

}
