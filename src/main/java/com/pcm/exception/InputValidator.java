package com.pcm.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InputValidator implements Validator{
	Logger logger = LoggerFactory.getLogger(InputValidator.class);
	

	public Integer isValidInteger(String input) {
		try{
			
			return Integer.parseInt(input);
		}catch(Exception e){
			logger.info("Error:Invalid Input. Not an Integer.");
		}
		return null;
	}

	public Double isValidDouble(String input) {
		try{			
			return Double.parseDouble(input);
		}catch(Exception e){
			logger.info("Error:Invalid Input. Not an Double.");
		}
		return null;
	}
	
	public Double isValidUnsignedDouble(String input) {
		try{			
			Double value = Double.parseDouble(input);
			return value >= 0 ? value : null;
		}catch(Exception e){
			logger.info("Error:Invalid Input. Value should be of type Double greater than zero.");
		}
		return null;
	}
	
	public Integer isValidUnsignedInteger(String input) {
		try{			
			Integer value = Integer.parseInt(input);
			return value >= 0 ? value : null;
		}catch(Exception e){
			logger.info("Error:Invalid Input. Value should be of type Integer greater than zero.");
		}
		return null;
	}
}
